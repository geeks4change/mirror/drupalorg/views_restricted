<?php

namespace Drupal\views_restricted\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines views_restricted annotation object.
 *
 * @Annotation
 */
class ViewsRestricted extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

}
